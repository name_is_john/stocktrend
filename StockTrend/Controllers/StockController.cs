using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using StockTrend.Interfaces;

namespace StockTrend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StockController : IStockTrendController
    {
        private List<string> closingPrices;
        private IStock populatedStock;
        private readonly IStockPriceService stockPriceService;

        public StockController(IStockPriceService stockPriceService)
        {
            this.stockPriceService = stockPriceService;
        }

        [HttpGet("{symbol}")]
        public ActionResult<IStock> Get(string symbol)
        {
            closingPrices = stockPriceService.GetClosingPrices(symbol);

            populatedStock = stockPriceService.SetStockValues(symbol, closingPrices);

            return new ActionResult<IStock>(populatedStock);
        }
    }
}