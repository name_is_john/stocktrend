using System;
using System.Collections.Generic;
using StockTrend.Enums;
using StockTrend.Interfaces;

namespace StockTrend.Services
{
    public class StockPriceService : IStockPriceService
    {
        private readonly IStockPriceClient<ITimeSeries> stockPriceClient;
        private ITimeSeries retrievedStockPrices;
        private List<string> stockClosingPrices;
        private IStock stock;

        public StockPriceService(IStock stock, IStockPriceClient<ITimeSeries> stockPriceClient)
        {
            stockClosingPrices = new List<string>();
            this.stock = stock;
            this.stockPriceClient = stockPriceClient;
        }

        public List<string> GetClosingPrices(string symbol)
        {
            stockPriceClient.Connect();

            if (stockPriceClient.IsConnected())
            {
                retrievedStockPrices = stockPriceClient.GetDailyStockPrices(symbol);
            }

            if (SuccessfullyRetrievedStockPrices(retrievedStockPrices))
            {
                ParseForClosingPrices(retrievedStockPrices);
            }

            return stockClosingPrices;
        }

        public IStock SetStockValues(string symbol, List<string> closingPrices)
        {
            stock.Symbol = symbol;

            if (EnoughClosingPricesToComputeTrend(closingPrices))
            {
                //Avapi API returns string values. Converting for calculations.
                stock.DayOneClosingPrice =
                    Convert.ToDecimal(ClosingPriceAtDayOneIndex(closingPrices));

                stock.LastClosingPrice =
                    Convert.ToDecimal(ClosingPriceAtLastDayIndex(closingPrices));

                stock.PriceTrendPercentage = GetTrend();
            }

            return stock;
        }

        private decimal GetTrend()
        {
            return (stock.LastClosingPrice - stock.DayOneClosingPrice) / stock.LastClosingPrice * 100;
        }

        private string ClosingPriceAtLastDayIndex(List<string> closingPrices)
        {
            return closingPrices[(int) TradingDay.LastClosingPriceIndex];
        }

        private string ClosingPriceAtDayOneIndex(List<string> closingPrices)
        {
            return closingPrices[(int) TradingDay.DayOneClosingPriceIndex];
        }

        private bool EnoughClosingPricesToComputeTrend(List<string> closingPrices)
        {
            return closingPrices.Count > (int) TradingDay.DayOneClosingPriceIndex;
        }

        private bool SuccessfullyRetrievedStockPrices(ITimeSeries retrievedStockPrices)
        {
            return !retrievedStockPrices.IsError;
        }

        private void ParseForClosingPrices(ITimeSeries retrievedStockPrices)
        {
            var stockInformationFromDailyResponse = retrievedStockPrices.DailySeries.Data;

            for (int index = 0; index <= (int) TradingDay.DayOneClosingPriceIndex; index++)
            {
                var dailyStockPrices = stockInformationFromDailyResponse.TimeSeries[index];
                stockClosingPrices.Add(dailyStockPrices.close);
            }
        }
    }
}