namespace StockTrend.Enums
{
    public enum TradingDay
    {
        LastClosingPriceIndex = 0,
        DayOneClosingPriceIndex = 11
    }
}