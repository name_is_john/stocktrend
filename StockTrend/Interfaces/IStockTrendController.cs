using Microsoft.AspNetCore.Mvc;

namespace StockTrend.Interfaces
{
    public interface IStockTrendController
    {
        ActionResult<IStock> Get(string symbol);
    }
}