using System.Collections.Generic;

namespace StockTrend.Interfaces
{
    public interface IStockPriceService
    {
        List<string> GetClosingPrices(string symbol);
        IStock SetStockValues(string symbol, List<string> closingPrices);
    }
}