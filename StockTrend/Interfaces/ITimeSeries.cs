using Avapi.AvapiTIME_SERIES_DAILY;

namespace StockTrend.Interfaces
{
    public interface ITimeSeries
    {
        IAvapiResponse_TIME_SERIES_DAILY DailySeries { get; set; }
        
        bool IsError { get; set; }
    }
}