namespace StockTrend.Interfaces
{
    public interface IStock
    {
        string Symbol { get; set; }

        decimal DayOneClosingPrice { get; set; }
        
        decimal LastClosingPrice { get; set; }
        
        decimal PriceTrendPercentage { get; set; }
    }
}