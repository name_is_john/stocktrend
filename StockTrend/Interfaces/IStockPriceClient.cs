namespace StockTrend.Interfaces
{
    public interface IStockPriceClient<out T>
    {
        void Connect();
        bool IsConnected();
        T GetDailyStockPrices(string symbol);
    }
}