﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StockTrend.Controllers;
using StockTrend.Gateways;
using StockTrend.Interfaces;
using StockTrend.Models;
using StockTrend.Services;

namespace StockTrend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
                options.CacheProfiles.Add("Default30",
                    new CacheProfile
                    {
                        Duration = 30
                    })).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            Bind(services);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }

        private static void Bind(IServiceCollection services)
        {
            services.AddScoped<IStock, Stock>();
            services.AddScoped<ITimeSeries, TimeSeries>();
            services.AddScoped<IStockPriceClient<ITimeSeries>, StockPriceClient>();
            services.AddScoped<IStockPriceService, StockPriceService>();
            services.AddScoped<IStockTrendController, StockController>();
        }
    }
}