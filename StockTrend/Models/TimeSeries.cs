using Avapi.AvapiTIME_SERIES_DAILY;
using StockTrend.Interfaces;

namespace StockTrend.Models
{
    public class TimeSeries : ITimeSeries
    {
        public IAvapiResponse_TIME_SERIES_DAILY DailySeries { get; set; }
        
        public bool IsError { get; set; }
    }
}