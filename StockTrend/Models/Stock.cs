using StockTrend.Interfaces;

namespace StockTrend.Models
{
    public class Stock : IStock
    {
        public string Symbol { get; set; }

        public decimal DayOneClosingPrice { get; set; }
        
        public decimal LastClosingPrice { get; set; }
        
        public decimal PriceTrendPercentage { get; set; }
    }
}