using System;
using Avapi;
using Microsoft.Extensions.Configuration;
using StockTrend.Interfaces;

namespace StockTrend.Gateways
{
    public class StockPriceClient : IStockPriceClient<ITimeSeries>
    {
        private readonly IConfiguration configuration;
        private IAvapiConnection connectionToStockApi;
        private ITimeSeries timeSeries;

        public StockPriceClient(IConfiguration configuration, ITimeSeries timeSeries)
        {
            this.timeSeries = timeSeries;
            this.configuration = configuration;
        }

        public void Connect()
        {
            connectionToStockApi = AvapiConnection.Instance;

            connectionToStockApi.Connect(configuration["AvapiKey"]);
        }

        public bool IsConnected()
        {
            return connectionToStockApi != null;
        }

        public ITimeSeries GetDailyStockPrices(string stockSymbol)
        {
            var timeSeriesDaily = connectionToStockApi.GetQueryObject_TIME_SERIES_DAILY();

            try
            {
                timeSeries.DailySeries = timeSeriesDaily.Query(stockSymbol);
            }
            catch (Exception e)
            {
                timeSeries.IsError = true;
            }


            return timeSeries;
        }
    }
}