FROM microsoft/dotnet:2.2-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 54411

FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /src
COPY StockTrend/StockTrend.csproj StockTrend/
RUN dotnet restore StockTrend/StockTrend.csproj
COPY . .
WORKDIR /src/StockTrend
RUN dotnet build StockTrend.csproj -c Release -o /app

FROM build AS publish
RUN dotnet publish StockTrend.csproj -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "StockTrend.dll"]
