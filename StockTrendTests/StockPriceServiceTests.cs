using System.Collections.Generic;
using Bogus;
using Moq;
using NUnit.Framework;
using StockTrend.Interfaces;
using StockTrend.Models;
using StockTrend.Services;

namespace StockTrendTests
{
    [TestFixture]
    public class StockServiceTests
    {
        private Randomizer randomizer;
        private string stockSymbol;
        private decimal dayOneClosingPrice;
        private decimal lastClosingPrice;
        private IStock stock;
        private Mock<IStockPriceClient<ITimeSeries>> stockPriceClientMock;
        private IStockPriceService stockPriceService;

        [SetUp]
        public void Setup()
        {
            randomizer = new Randomizer();
            stockSymbol = randomizer.Word();
            dayOneClosingPrice = randomizer.Decimal();
            lastClosingPrice = randomizer.Decimal();
            stock = new Stock();
            stockPriceClientMock = new Mock<IStockPriceClient<ITimeSeries>>();
            stockPriceService = new StockPriceService(stock, stockPriceClientMock.Object);
        }

        [Test]
        public void SetStockValues_GivenStockSymbolInput_Sets_StockObjectSymbol()
        {
            stockPriceService.SetStockValues(stockSymbol, new List<string>());

            Assert.True(stock.Symbol == stockSymbol);
        }

        [Test]
        public void SetStockValues_GivenStockSymbolInput_Sets_StockObjectClosingPrices()
        {
            List<string> stockValueReturn = new List<string>
            {
                lastClosingPrice.ToString(),
                "2", "3", "4", "5", "6", "7", "8", "9", "10", "11",
                dayOneClosingPrice.ToString()
            };

            stockPriceService.SetStockValues(stockSymbol, stockValueReturn);

            Assert.True(stock.DayOneClosingPrice == dayOneClosingPrice);
            Assert.True(stock.LastClosingPrice == lastClosingPrice);
        }

        [Test]
        public void SetStockValues_GivenStockSymbolInput_Calculates_StockObjectClosingPriceTrend()
        {
            decimal testPriceTrendPercentage = (lastClosingPrice - dayOneClosingPrice) / lastClosingPrice * 100;
            List<string> stockValueReturn = new List<string>
            {
                lastClosingPrice.ToString(),
                "2", "3", "4", "5", "6", "7", "8", "9", "10", "11",
                dayOneClosingPrice.ToString()
            };

            stockPriceService.SetStockValues(stockSymbol, stockValueReturn);

            Assert.True(stock.PriceTrendPercentage == testPriceTrendPercentage);
        }

        [Test]
        public void SetStockValues_NotGivenEnoughClosingPricesToComputeTrend_DoesNotSetStockProperties()
        {
            var defaultDecimalValue = 0;

            List<string> stockValueReturn = new List<string>
            {
                "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"
            };

            stockPriceService.SetStockValues(stockSymbol, stockValueReturn);

            Assert.True(stock.Symbol == stockSymbol);
            Assert.True(stock.DayOneClosingPrice == defaultDecimalValue);
            Assert.True(stock.LastClosingPrice == defaultDecimalValue);
            Assert.True(stock.PriceTrendPercentage == defaultDecimalValue);
        }
    }
}