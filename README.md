#Stock Trend API

ASP.Net Core Api that allows for a user to enter a stock symbol to retrieve the last closing price and the price trend for the last 12 days. 

##Docker Support:

Please ensure that Docker is installed on your local machine. If not installed follow the appropriate link below;
[Windows](https://docs.docker.com/docker-for-windows/install/)
[Mac](https://docs.docker.com/docker-for-mac/install/)

To create and start the container run the following;

- docker-compose build
- docker-compose --no-ansi up -d  --force-recreate --remove-orphans

The following commands allow you to stop and remove the container.

- docker-compose stop stocktrend
- docker-composer rm stocktrend

##Getting Stock Data:

- curl 0.0.0.0:54421/api/stock/msft

Replace msft with any symbol that you would like to gather stock trading data from.
